var noble = require('noble');


//########################
console.log('noble');

var myPeripheral;
var peripheralName = "LED";
var myMac;

//exports.myMac = myMac;
exports.noble = noble;

// here we start scanning. we check if Bluetooth is on
noble.on('stateChange', scan);
//myMac = noble.localAddress;

function scan(state){
  if (state === 'poweredOn') {
    noble.startScanning([],false);
    //myMac = noble.address;
    console.log("Started scanning");
  } else {
    noble.stopScanning();
    console.log("Is Bluetooth on?");
  }
}

/*
noble.on(‘discover’, function(peripheral) {

  var macAddress = peripheral.uuid;
  var rss = peripheral.rssi;
  var localName = advertisement.localName;
  console.log('found device: ', macAdress, ' ', localName, ' ', rss);
}
*/

//noble.on('discover', discoverPeripherals);

function discoverPeripherals(peripheral) {
  //here we check if this is the peripheral we want

  if(peripheral.advertisement.localName == peripheralName){
    console.log("found my device");

    //stop scanning for other devices
    noble.stopScanning();

	  //save peripheral  to a variable
    myPeripheral = peripheral;

    //connect to peripheral
    peripheral.connect(explorePeripheral);
  }
  else{
    console.log("found a different device with UUID "+ peripheral.uuid);
    //console.log(peripheral.uuid + ' RSSI updated : ' + peripheral.rssi);
  }
};


function explorePeripheral(error) {
  console.log("connected to "+myPeripheral.advertisement.localName);

  //console log signal strengh every second
  setInterval(updateRSSI, 60);

  //when disconnected, run this function
  myPeripheral.on('disconnect', disconnectPeripheral);
};

function updateRSSI(){
    myPeripheral.updateRssi(function(error, rssi){
    //rssi are always negative values
    if(rssi < 0) console.log("here is my RSSI: "+rssi);
  });

}

function disconnectPeripheral(){
      console.log('peripheral disconneted');

      //stop calling updateRSSI
      clearInterval(updateRSSI);

      //restart scan
      noble.startScanning();
}
//Contact GitHub API Training Shop Blog About
