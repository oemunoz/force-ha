// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

//var d3 = require("d3");
//var noble = require('noble');   //noble library
//var noble = require('frameless_window.js');
//var rrdtool = require('rrdtool');
window.$ = window.jQuery = require('jquery');

//var bindings = require('resolve-bindings')();
var force = require('./force.js');
force.drawGraph();
graph = force.graph;
//graph.addNode('oems');
//graph.addLink('oems', 'Oscar', '15');

//var myNoble = require('./noble.js');
//myNoble.noble.startScanning([],false);
//console.log(myNoble.myMac);
//myNoble.updateRSSI();

//var noble = require('./noble.js');
//var bleno = require('./bleno.js')
var bleno = require('bleno');
var util = require('util');
var os = require('os');
//var eddystone = require('eddystone-beacon');
var BlenoPrimaryService = bleno.PrimaryService;
var BlenoCharacteristic = bleno.Characteristic;

//require('./rrdtool.js');

/// frameless_window
function updateCheckbox() {
  var top_checkbox = document.getElementById("top-box");
  var bottom_checkbox = document.getElementById("bottom-box");
  var left_checkbox = document.getElementById("left-box");
  var right_checkbox = document.getElementById("right-box");
  if (top_checkbox.checked || bottom_checkbox.checked) {
    left_checkbox.disabled = true;
    right_checkbox.disabled = true;
  } else if (left_checkbox.checked || right_checkbox.checked) {
    top_checkbox.disabled = true;
    bottom_checkbox.disabled = true;
  } else {
    left_checkbox.disabled = false;
    right_checkbox.disabled = false;
    top_checkbox.disabled = false;
    bottom_checkbox.disabled = false;
  }
}

function initCheckbox(checkboxId, titlebar_name, titlebar_icon_url, titlebar_text) {
  var elem = document.getElementById(checkboxId);
  if (!elem)
    return;
  elem.onclick = function() {
    if (document.getElementById(checkboxId).checked)
      addTitlebar(titlebar_name, titlebar_icon_url, titlebar_text);
    else
      removeTitlebar(titlebar_name);
    focusTitlebars(true);

    updateContentStyle();
    updateCheckbox();
  }
}

var aside = document.querySelector('.sidebar');
var mainContainer = document.querySelectorAll('.content-wrapper');
var switcher = document.getElementById('switcher');

//switcher.addEventListener('click', slide, false);

function slide() {
  aside.classList.add('transition-divs');
  aside.classList.toggle('aside-left');
  [].forEach.call(mainContainer, function(c) {
    c.classList.add('transition-divs');
    c.classList.toggle('centering');
  });
}

window.onfocus = function() {
  console.log("focus");
  focusTitlebars(true);

}

window.onblur = function() {
  console.log("blur");
  focusTitlebars(false);
}

window.onresize = function() {
  updateContentStyle();
  slide();
}

window.onload = function() {
  initCheckbox("top-box", "top-titlebar", "top-titlebar.png", "Top Titlebar");
  initCheckbox("bottom-box", "bottom-titlebar", "bottom-titlebar.png", "Bottom Titlebar");
  initCheckbox("left-box", "left-titlebar", "left-titlebar.png", "Left Titlebar");
  initCheckbox("right-box", "right-titlebar", "right-titlebar.png", "Right Titlebar");

  const {remote} = require('electron');
  const {BrowserWindow} = remote;
  const win = BrowserWindow.getFocusedWindow();

  document.getElementById("transition-divs").onclick = function() {
    slide();
  }
  /*document.getElementById("close-window-button").onclick = function() {
  //  win.close();
  //}
  document.getElementById("minimize-window-button").onclick = function() {
    win.minimize();
  }
  document.getElementById("maximize-window-button").onclick = function() {
    win.maximize();
  }
  document.getElementById("unmaximize-window-button").onclick = function() {
    win.unmaximize();
  }
  document.getElementById("toggle-window-button").onclick = function() {
    win.setFullScreen(!(win.isFullScreen()));
  }
  document.getElementById("maxmin-window-button").onclick = function() {
    win.isMaximized() ? win.unmaximize() : win.maximize();
  }
  /*document.getElementById("min").onclick = function() {
    win.minimize();
  }
  document.getElementById("max").onclick = function() {
    win.isMaximized() ? win.unmaximize() : win.maximize();
  }*/
  document.getElementById("exit").onclick = function() {
    win.close();
  }

  // Characteristic

  var EchoCharacteristic = function() {
    EchoCharacteristic.super_.call(this, {
      uuid: 'ec0e',
      properties: ['read', 'write', 'notify'],
      value: null
    });

    this._value = new Buffer(0);
    this._updateValueCallback = null;
  };

  util.inherits(EchoCharacteristic, BlenoCharacteristic);

  EchoCharacteristic.prototype.onReadRequest = function(offset, callback) {
    console.log('EchoCharacteristic - onReadRequest: value = ' + this._value.toString('hex'));

    callback(this.RESULT_SUCCESS, this._value);
  };

  EchoCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {
    this._value = data;

    console.log('EchoCharacteristic - onWriteRequest: value = ' + this._value.toString('hex'));

    if (this._updateValueCallback) {
      console.log('EchoCharacteristic - onWriteRequest: notifying');

      this._updateValueCallback(this._value);
    }

    callback(this.RESULT_SUCCESS);
  };

  EchoCharacteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
    console.log('EchoCharacteristic - onSubscribe');

    this._updateValueCallback = updateValueCallback;
  };

  EchoCharacteristic.prototype.onUnsubscribe = function() {
    console.log('EchoCharacteristic - onUnsubscribe');

    this._updateValueCallback = null;

    // Bleno doesn't fire 'disconnect' on OS X
    // Assume the client disconnected & start advertising beacon again
    // on Linux, bleno.on('disconnect', callback) is be better (see below)
    if (os.platform() === 'darwin') {
      startBeacon();
    }
  };

  function calculateDistance(rssi) {

    var txPower = -59 //hard coded power value. Usually ranges between -59 to -65

    if (rssi == 0) {
      return -1.0;
    }

    var ratio = rssi*1.0/txPower;
    if (ratio < 1.0) {
      return Math.pow(ratio,10);
    }
    else {
      var distance =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
      return distance;
    }
  }

  // Sevice

  console.log('bleno - echo');

  var address_log = function(error,rssi) {
    distance = calculateDistance(rssi);
    console.log('Rssi:Error:' + calculateDistance(rssi) + ':' + error);
    //eddystone.advertiseUrl('http://example.com/echo');
  }

  bleno.on('stateChange', function(state) {
    console.log('on -> stateChange: ' + state);

    if (state === 'poweredOn') {
      bleno.startAdvertising('echo', ['ec00']);
      startBeacon();
    } else {
      bleno.stopAdvertising();
    }
  });

  bleno.on('advertisingStart', function(error) {
    console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

    if (!error) {
      bleno.setServices([
        new BlenoPrimaryService({
          uuid: 'ec00',
          characteristics: [
            new EchoCharacteristic()
          ]
        })
      ]);
    }
  });

  // Beacon

  var error,rssi,distance,mac;

  var rssi_log = function(error,rssi) {
    distance = calculateDistance(rssi);
    //console.log('Rssi:Error:' + calculateDistance(rssi) + ':' + error);
    console.log('mac:distance: ' + mac + calculateDistance(rssi));
    graph.addNode(mac);
    distance = (distance * 5);
    if(distance <= 50){
      console.log('mac:final_distance: ' + mac + distance);
      graph.addLink('00:1A:7D:DA:71:11', mac, distance);
    }else{
      graph.addLink('00:1A:7D:DA:71:11', mac, 50);
    }
    //eddystone.advertiseUrl('http://example.com/echo');
  }

  function startBeacon() {
    console.log("Starting beacon.");
    //eddystone.advertiseUrl('http://example.com/echo');
  }

  bleno.on('accept', function(clientAddress) {
    console.log("Accepting connection. Stopping beacon:");
    mac = clientAddress;
    bleno.updateRssi(rssi_log);
    //eddystone.stop();
  });

  /*
  bleno.on('rssiUpdate', function(rssi,error) {
    console.log(rssi + '::' +error);
  });
  */

  // disconnect is only fired on Linux
  // see EchoCharacteristic.onUnsubscribe for the OS X hack
  bleno.on('disconnect', function() {
    console.log("Client disconnected ...");
    startBeacon();
  })

  updateContentStyle();


}
