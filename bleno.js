var bleno = require('bleno');
var util = require('util');
var os = require('os');
//var eddystone = require('eddystone-beacon');

var BlenoPrimaryService = bleno.PrimaryService;
var BlenoCharacteristic = bleno.Characteristic;

// Characteristic

var EchoCharacteristic = function() {
  EchoCharacteristic.super_.call(this, {
    uuid: 'ec0e',
    properties: ['read', 'write', 'notify'],
    value: null
  });

  this._value = new Buffer(0);
  this._updateValueCallback = null;
};

util.inherits(EchoCharacteristic, BlenoCharacteristic);

EchoCharacteristic.prototype.onReadRequest = function(offset, callback) {
  console.log('EchoCharacteristic - onReadRequest: value = ' + this._value.toString('hex'));

  callback(this.RESULT_SUCCESS, this._value);
};

EchoCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {
  this._value = data;

  console.log('EchoCharacteristic - onWriteRequest: value = ' + this._value.toString('hex'));

  if (this._updateValueCallback) {
    console.log('EchoCharacteristic - onWriteRequest: notifying');

    this._updateValueCallback(this._value);
  }

  callback(this.RESULT_SUCCESS);
};

EchoCharacteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('EchoCharacteristic - onSubscribe');

  this._updateValueCallback = updateValueCallback;
};

EchoCharacteristic.prototype.onUnsubscribe = function() {
  console.log('EchoCharacteristic - onUnsubscribe');

  this._updateValueCallback = null;

  // Bleno doesn't fire 'disconnect' on OS X
  // Assume the client disconnected & start advertising beacon again
  // on Linux, bleno.on('disconnect', callback) is be better (see below)
  if (os.platform() === 'darwin') {
    startBeacon();
  }
};

function calculateDistance(rssi) {

  var txPower = -59 //hard coded power value. Usually ranges between -59 to -65

  if (rssi == 0) {
    return -1.0;
  }

  var ratio = rssi*1.0/txPower;
  if (ratio < 1.0) {
    return Math.pow(ratio,10);
  }
  else {
    var distance =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
    return distance;
  }
}

// Sevice

console.log('bleno - echo');

var address_log = function(error,rssi) {
  distance = calculateDistance(rssi);
  console.log('Rssi:Error:' + calculateDistance(rssi) + ':' + error);
  //eddystone.advertiseUrl('http://example.com/echo');
}

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    bleno.startAdvertising('echo', ['ec00']);
    startBeacon();
  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {
  console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

  if (!error) {
    bleno.setServices([
      new BlenoPrimaryService({
        uuid: 'ec00',
        characteristics: [
          new EchoCharacteristic()
        ]
      })
    ]);
  }
});

// Beacon

var error,rssi,distance,mac;

var rssi_log = function(error,rssi) {
  distance = calculateDistance(rssi);
  //console.log('Rssi:Error:' + calculateDistance(rssi) + ':' + error);
  console.log('mac:distance: ' + mac + calculateDistance(rssi));
  //eddystone.advertiseUrl('http://example.com/echo');
}

function startBeacon() {
  console.log("Starting beacon.");
  //eddystone.advertiseUrl('http://example.com/echo');
}

bleno.on('accept', function(clientAddress) {
  console.log("Accepting connection. Stopping beacon:");
  mac = clientAddress;
  bleno.updateRssi(rssi_log);
  //eddystone.stop();
});

/*
bleno.on('rssiUpdate', function(rssi,error) {
  console.log(rssi + '::' +error);
});
*/

// disconnect is only fired on Linux
// see EchoCharacteristic.onUnsubscribe for the OS X hack
bleno.on('disconnect', function() {
  console.log("Client disconnected ...");
  startBeacon();
})




/*
var bleno = require('bleno');

var BlenoPrimaryService = bleno.PrimaryService;

var EchoCharacteristic = require('./characteristic');

console.log('bleno - echo');

bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    bleno.startAdvertising('echo', ['ec00']);

  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {
  console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

  if (!error) {
    bleno.setServices([
      new BlenoPrimaryService({
        uuid: 'ec00',
        characteristics: [
          new EchoCharacteristic()
        ]
      })
    ]);
  }
});
*/
