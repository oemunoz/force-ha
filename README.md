# ForceHA

**Bluetooth aplication.**

Aplication build on [Electron] (http://electron.atom.io/docs/latest/tutorial/quick-start).

**[https://gitlab.com/iush](https://gitlab.com/iush) WebSite.**

You need to install nodejs and npm for run this developer version.

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone git@gitlab.com:oemunoz/force-ha.git
# Go into the repository
cd force-ha
# Install dependencies and run the app
npm install && npm start
```

Learn more about Electron and its API in the [documentation](https://gitlab.com/iush).

[![Emparejamiento](https://gitlab.com/iush/hardware-architecture/raw/master/images/app.png "Emparejamiento")](https://gitlab.com/oemunoz/force-ha)

#### Useful links
- [Bleno](https://github.com/sandeepmistry/bleno)
- [D3](https://github.com/sandeepmistry/bleno)

#### License [MIT](https://opensource.org/licenses/MIT)
